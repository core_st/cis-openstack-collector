FROM oscimage
ADD oscollector /opt/collectors/osc
WORKDIR /opt/collectors/osc
ENTRYPOINT ["python"]
CMD ["runner.py"]
