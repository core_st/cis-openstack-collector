class CollectorError(Exception):
    pass


class ServiceCollectorError(Exception):
    pass


class ValidationError(Exception):
    pass
